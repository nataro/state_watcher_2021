from logging import getLogger, StreamHandler, DEBUG
import threading
from datetime import datetime
from pathlib import Path
import random
import time

from typing import Sequence, Optional, Dict, List, Tuple

import numpy as np

import cv2

from utils.ar_clipper import ArClipper
from utils.process_state import StateContext

from ml.dl_model import Fai2Classifier, AlbumentationsTransform

from th.device_handling import DeviceHandlingBuilder
from dev.cam_capture import CaptureDevice
from th.xbee_director import XbeeHandlingDirector

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)


class CamHandlingBuilder(DeviceHandlingBuilder):
    """
    usbカメラ等を用いて画像を撮影する

        * 取得した画像をマーカを基準として指定した範囲で切り取る
            * 切り取りのツールはutils.ar_clipper.ArClipperを用いる
                ArClipperには、切り取りマーカーや切り取り範囲の設定などが含まれる

        * シングルマーカーを検出し、基準となるマーカーボードを原点とした座標に換算
            * 指定した閾値より原点側にあるシングルマーカーのidをxbee送信

    """

    def __init__(
            self,
            device: CaptureDevice,
            ar_clipper=ArClipper(),
            xbee_dr: XbeeHandlingDirector = None,
            cc_interval_sec: int = 30,
            sm_thresh_m: float = 0.4,
            sm_thresh_coord: int = 0,
    ):
        """
        :param device: usbカメラデバイス
        :param ar_clipper: ARマーカ基準で画像を切り取るインスタンス
        :param xbee_dr: xbee送信デバイス制御のXbeeHandlingDirector
        :param cc_interval_sec: Communication confirmation（疎通確認間隔）
        ↓シングルマーカー検出に係わる引数
        :param sm_thresh_m: マーカーボードによる基準原点近傍と判断する閾値をmで指定
        :param sm_thresh_coord: 座標の閾値をどの軸で判定するか　0->x軸, 1->y軸
        """
        super().__init__(device)
        self._ar_clipper = ar_clipper
        '''xbee送信に係わるメンバ変数'''
        self._xbee_dr = xbee_dr
        self._xbee_sent_time = time.time()  # xbeeで送信した時刻を保持しておく（疎通確認間隔チェック用）
        self._xbee_sent_idx = 0  # xbeeで送信した状態データ（0から3の数値）を保持しておく
        self._cc_interval = cc_interval_sec  # 疎通確認通信の間隔
        '''状態推定に係わるメンバ変数'''
        model_path = '/home/seisan/prg/state_watcher/ml/trained_model.pkl'
        self._classifier = Fai2Classifier(path=model_path, size=128)
        # self._classifier = None  # win test
        # ステートパターンで4つの状態を管理するインスタンス
        # self._context.state_keys -> ['00_nowork', '01_newwork', '02_processing', '03_handling']
        self._context = StateContext(buffer_num=5)
        '''シングルマーカ座標判定に係わるメンバ変数'''
        self._neighborhood_thresh = sm_thresh_m
        self._coord_idx = sm_thresh_coord

    @property
    def xbee_dr(self):
        return self._xbee_dr

    @xbee_dr.setter
    def xbee_dr(self, xbee_dr: XbeeHandlingDirector):
        self._xbee_dr = xbee_dr

    def handle_via_device(self,
                          lock: threading.Lock,
                          dir_path: str = '',
                          ext: str = 'jpg',
                          ):
        """
        DeviceHandlingBuilder.hadling_thred(self, **kwargs)において以下のように呼び出される
            # 辞書型であるkwargsを、キーワード引数として渡したいので**kwargsとする
            self._executor.submit(self.handle_via_device, self._lock, **kwargs)

            切り取り画像が無い（マーカー検出できていない）：data=None

            ワーク無し：c_name='00_nowork'
            新ワーク：c_name='01_newwork'
            加工中：c_name='02_processing'
            作業中：c_name='03_handling'

            #変化が無い場合はNone、変化があった場合は投票結果が返ってくる
            state_key
        """
        logger.debug(self.__class__.__name__ + ' : thread start')
        with lock:  # deviceへのアクセスを制限
            logger.debug(self.__class__.__name__ + ' : lock')

            # ここで画像取得
            img = self._device()
            if img is None: return

            # self.ar_clipperに画像を渡し、検出マーカ情報を更新する
            # （検出したマーカーの情報はself._ar_clipperに保持される）
            # この後の画像切り取りや、マーカー座標取得は、ここで検出されたマーカ情報を基に実施される
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            self._ar_clipper.update_detect_markers(gray)

            ''' 画像の状態推定 '''
            # 事前にself._ar_clipper.update_detect_markers(gray)を実施しておくこと
            data_cl = self._ar_clipper.get_origin_clipped_image(img)
            # 矩形画像の場合、正方形にpaddingする
            data_cl = self.square_padding(data_cl)
            if data_cl is not None:
                # クラス分類結果を文字列で得る
                c_name = self._classifier(data_cl)
                # logger.debug(f"cam_hd -> classified:{c_name}")
                # stateの遷移と確認
                self._context.feed_state_info(c_name)
                state_key = self._context.state_transition()
                logger.info(f"{state_key},{c_name}")

                try:
                    if state_key is not None:
                        # 状態変化が起きた場合は必ず送信されるようにする
                        self._xbee_sent_time = time.time() - self._cc_interval
                        # xbeeの送信済みindexを更新
                        self._xbee_sent_idx = self._context.state_keys.index(state_key)
                        # ファイル名にクラス分離結果を追加して画像を保存する
                        self.classified_image_writer(data_cl, c_name, dir_path, ext)

                    # 疎通確認間隔を確認してxbeeで送信
                    if time.time() - self._xbee_sent_time >= self._cc_interval:
                        # 波括弧のエスケープは「{{」波括弧を２回繰り返す
                        self._xbee_dr(f'{{"state":{self._xbee_sent_idx}}}')
                        self._xbee_sent_time = time.time()  # 送信時刻の更新

                except Exception as e:
                    logger.error(f"{self.__class__.__name__} :  {e}")

                # # テスト用（全結果送信）
                # idx = self._context.state_keys.index(c_name)
                # self._xbee_dr(f'{{"state":{idx}}}')

                # # 本番用
                # if state_key is not None:
                #     try:
                #         # xbeeで送信
                #         idx = self._context.state_keys.index(state_key)
                #         # 波括弧のエスケープは「{{」波括弧を２回繰り返す
                #         # print(f'{{"state":{idx}}}')
                #         self._xbee_dr(f'{{"state":{idx}}}')
                #
                #         # ファイル名にクラス分離結果を追加して保存する
                #         self.classified_image_writer(data, c_name, dir_path, ext)
                #     except Exception as e:
                #         logger.error(f"{self.__class__.__name__} :  {e}")

            '''シングルのARマーカ取得'''
            # 事前にself._ar_clipper.update_detect_markers(gray)を実施しておくこと
            detect_markers = self._ar_clipper.get_origin_single_markers()

            if detect_markers:  # 検出されなかったときは空のリストが返る
                # coordに含まれているx,y,x値のうち、指定した軸の値が最も小さいものを取り出す
                coord, euler, id = self.nearest_marker(detect_markers, coord_idx=self._coord_idx)

                if coord[self._coord_idx] < self._neighborhood_thresh:  # 指定した軸の距離を閾値判定
                    try:
                        time.sleep(0.2)  # 少し待機

                        # 波括弧のエスケープは「{{」波括弧を２回繰り返す
                        self._xbee_dr(f'{{"tool_state":{id}}}')
                        # self._xbee_dr(f'{{"state":{id}}}')

                    except Exception as e:
                        logger.error(f"{self.__class__.__name__} :  {e}")

        logger.debug(self.__class__.__name__ + ' : unlock')

    @staticmethod
    def nearest_marker(detect_maekers: List[Tuple], coord_idx: int = 0):
        """
        ar_org_neighborhood_markers()から出力された
        detect_markers = [
            (coord, euler, id),
            ...,
            (coord, euler, id),
        ]
        の coord=(x,y,z) のうち、xoord_idxで指定したインデックスの数値を比較して最小のものを返す
        :param coord_idx: (0～2)で軸を指定（x,y,z）
        :return:
        """
        coords = [m[coord_idx] for m in detect_maekers]
        min_idx = coords.index(min(coords))
        return detect_maekers[min_idx]

    def classified_image_writer(self,
                                img: np.ndarray,
                                class_name: str,
                                dir_path: str,
                                ext: str = 'jpg',
                                limit_img_num: int = 30):
        """
        画像データと分類したクラス名を渡して、そのクラス名をファイル名に追加して画像データを保存する
        保存するディレクトリ内に同じクラスのファイルがlimit_img_num以上ある場合、それ以上画像を保存しない
        """
        if img is None: return

        # 　ファイル名の先頭にクラス名を追加
        dt = datetime.now()
        file_path = dir_path + "/{0}_{1}.".format(class_name, dt.strftime("%Y%m%d-%H%M%S-%f")) + ext

        # ディレクトリ内に同じクラス名のファイルが何個あるか確認する
        file_num = self.get_files_num(dir_path, str(class_name))

        # 同じクラスのファイルは個数を確認して保存するかどうか判断する
        if file_num <= limit_img_num:
            cv2.imwrite(file_path, img)

    @staticmethod
    def get_files_num(dir_path, file_name: str) -> int:
        """
        ディレクトリに含まれるファイルの内、ファイル名の一部が一致するファイルの個数をカウントする
        Parameters
        ----------
        dir_path:検索対象となるディレクトリ
        file_name:検索するファイル名の一部

        Returns
        -------
        該当するファイルの個数
        """
        path_ob = Path(dir_path)
        file_name_list = [str(p) for p in path_ob.glob('*')]
        # 取得した各ファイル名を指定した文字でスプリットし、その要素数をカウントしていく
        # 該当する文字が含まれるファイル名は2以上となり、該当しない場合は1となる
        sep_cont_list = [len(f.split(file_name)) for f in file_name_list]
        return sum(sep_cont_list) - len(file_name_list)

    @staticmethod
    def square_padding(img: Optional[np.ndarray]) -> Optional[np.ndarray]:
        """
        正方形の画像にパディングする
        Parameters
        ----------
        img

        Returns
        -------
        """
        if img is None: return None
        mean = (0.485, 0.456, 0.406)
        value = [int(i * 255) for i in mean]
        h, w, *c = img.shape
        sq_size = max([h, w])
        top = (sq_size - h) // 2
        bottom = sq_size - h - top
        left = (sq_size - w) // 2
        right = sq_size - w - left

        sq_img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=value)

        return sq_img


class CamHandlingDirector(object):
    def __init__(
            self,
            builder: CamHandlingBuilder,
            dir_path: str = None,
            xbee_dr: XbeeHandlingDirector = None,
    ):
        self._builder = builder
        self._storage_path = self.create_storage_directory(dir_path)
        self._xbee_dr = xbee_dr  # CamHandlingBuilder内で使用するxbeeデバイス
        if self._xbee_dr is not None:
            self._builder.xbee_dr = self._xbee_dr

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        logger.debug(self.__class__.__name__ + ': __call__() called : ')
        self.handling()

    def handling(self) -> None:
        self._builder.handling_thread(dir_path=self._storage_path)

    def device_open(self) -> bool:
        if self._xbee_dr is not None:
            self._xbee_dr.device_open()
        return self._builder.device_open()

    def device_close(self) -> None:
        if self._xbee_dr is not None:
            self._xbee_dr.device_close()
        self._builder.device_close()

    def executor_shutdown(self) -> None:
        if self._xbee_dr is not None:
            self._xbee_dr.executor_shutdown()
        self._builder.executor_shutdown()  # ThreadPoolを閉じる

    @staticmethod
    def create_storage_directory(dir_path: str = None) -> str:
        """
        インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）

        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()

        dt = datetime.now()
        y = dt.strftime('%Y')
        m = dt.strftime('%m')
        d = dt.strftime('%d')

        p_dir_path = p_dir_path / y / m / d

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    import time

    from dev.xbee_router import XbeeSenderDevice
    from th.xbee_director import XbeeHandlingBuilder, XbeeHandlingDirector

    # CamHandlingDirectorに渡すxbeeを用意(必須ではない)
    PORT = 'COM7'
    BAUD = 9600
    xb_dev = XbeeSenderDevice(PORT, BAUD)
    xb_hb = XbeeHandlingBuilder(xb_dev)
    xb_dc = XbeeHandlingDirector(xb_hb)

    cap_dev = CaptureDevice(cam_num=0)
    cap_hd = CamHandlingBuilder(cap_dev)
    # dir_path = '/mnt/hdd/satahdd'
    # cap_dc = CamHandlingDirector(cap_hd,dir_path=dir_path)
    # cap_dc = CamHandlingDirector(cap_hd)
    cap_dc = CamHandlingDirector(cap_hd, xbee_dr=xb_dc)

    print('open')
    cap_dc.device_open()

    time.sleep(1)

    cap_dc()

    time.sleep(10)  # 計測が実行される前にcloseするとエラーになるので待機

    cap_dc.device_close()
    cap_dc.executor_shutdown()

    print('closed')
