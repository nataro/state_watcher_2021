
from logging import getLogger, StreamHandler
import threading
from concurrent.futures import ThreadPoolExecutor

from datetime import datetime
from abc import ABCMeta, abstractmethod
import json
from typing import Sequence, Optional, Dict
from pathlib import Path

import numpy as np
import cv2

from dev.callable_device import CallableDevice
from dev.xbee_router import XbeeSenderDevice
from dev.cam_capture import CaptureDevice
from utils.ar_clipper import ArClipper

# logger = getLogger(__name__)
# handler = StreamHandler()
# logger.addHandler(handler)


class DeviceHandlingBuilder(metaclass=ABCMeta):
    """
    ここを参考に
    https://qiita.com/__init__/items/74b36eba31ccbc0364ed
    """

    def __init__(self, device: CallableDevice, max_workers: int = 3):
        self._device = device
        self._executor = ThreadPoolExecutor(max_workers=max_workers)
        self._lock = threading.Lock()  # 複数のスレッドが同時に録音デバイスにアクセスしないようにロックする

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    def executor_shutdown(self) -> None:
        self._executor.shutdown()  # ThreadPoolを閉じる

    def handling_thread(self, **kwargs):
        """
        Parameters
        ----------
        kwargs
            キーワード引数で受け取ったkwargsはDict型になる
        -------
        """
        # self._executor.submit(self.handle_via_device, self._lock, kwargs)
        # 辞書型であるkwargsを、キーワード引数として渡したいので**kwargsとする
        self._executor.submit(self.handle_via_device, self._lock, **kwargs)

    @abstractmethod
    def handle_via_device(self,
                          lock: threading.Lock,
                          **kwargs, ):
        # raise NotImplementedError()
        pass


class DeviceHandlingBuilderReceive(DeviceHandlingBuilder):
    """
    thredを実行する際に辞書型などで情報を渡して、その情報をthred処理内で利用する場合、
    受け取った辞書型の内容についてバリデーションを行うための関数を追加したもの。
    """

    @abstractmethod
    def check_dict(self, received: Dict) -> str:
        pass

    @staticmethod
    def keys_check(target_keys: Sequence, must_keys: Sequence) -> bool:
        """
        target_keysの中に、must_keysが全部含まれているか確認する
        """
        return set(target_keys) >= set(must_keys)


