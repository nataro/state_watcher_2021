import time
from datetime import datetime

import logging

from tornado.ioloop import IOLoop

from dev.cam_capture import CaptureDevice

from th.cam_director import CamHandlingDirector, CamHandlingBuilder

from utils import logger_tool

## logging setting
logging.basicConfig(level=logging.ERROR, format='%(threadName)s: %(message)s')

# file_name_head
dt = datetime.now()
logfile_head = './log_file/' + dt.strftime('%Y%m%d_')
# logfile_head = '/home/seisan/prg/state_watcher/log_file/' + dt.strftime('%Y%m%d_')

# cam_direcotr log
logger_name = 'th.cam_director'
log_file = 'cam_dr.log'
fm = '%(asctime)s,%(message)s'
date_fm = '%Y-%m-%d %H:%M:%S'
logger_tool.get_logger_with_presets(
    logger_name=logger_name,
    log_file=logfile_head + log_file,
    format=fm,
    date_format=date_fm,
    # level=logging.DEBUG,
    level=logging.INFO,
)
# xbee_director log
logger_name = 'th.xbee_director'
log_file = 'xbee_dr.log'
fm = '%(asctime)s:%(name)s:%(levelname)s:%(message)s'
logger = logger_tool.get_logger_with_presets(
    logger_name=logger_name,
    # log_file=logfile_head + log_file,
    format=fm,
    # level=logging.DEBUG,
    level=logging.ERROR,
)

"""
PollingServerが定期的にself._cap_director(RecordManagerDirector)を動作させるようになっている

CamHandlingDirectorクラスで
    * DLモデルの読み込みファイル指定
    * 保存する画像の個数制限
    * クリッピングした画像の正方形パディングを行っている

また、ARマーカーを利用したクリッピングは、utils.ar_clipper.pyのArClipperクラスに直接切り取り範囲を指定して書いている

画像データの保存場所は、PollingServerのインスタンス生成時に渡す,self._cap_director(CamHandlingDirector)
を生成する際に指定すれば良い。

"""


class PollingServer(object):
    """
    """

    def __init__(self,
                 polling_hz: float = 1,
                 cap: CamHandlingDirector = None,
                 ):
        """
        Parameters
        ----------
        polling_hz : float
            ポーリングの実行周期[hz]
        """
        self._io_loop = IOLoop.current()
        self._loop_freq = polling_hz

        # データロギングを管理するクラス
        self._cap_director = cap

        self.loop()  # 定義したループ処理を実行

    def __enter__(self):
        """
        with 文開始時にコールされる。
        ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
        """
        # カメラを開く前に少しタイムラグを置かないとエラーになる
        time.sleep(5)
        self._cap_director.device_open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
        例外が発生しないときはすべて None がセットされる
        """
        self._cap_director.device_close()

    def loop(self):
        """再帰的に自分自身を呼び出してループ処理"""
        self._io_loop.call_later(1 / self._loop_freq, self.loop)  # こっちでもよさそう
        self.callback()

    def callback(self, receive_segment: bytes = None):
        """
        Parameters
        ----------
        receive_segment : bytes

        """
        self._cap_director()


if __name__ == "__main__":
    from ml.dl_model import AlbumentationsTransform
    from dev.xbee_router import XbeeSenderDevice
    from th.xbee_director import XbeeHandlingBuilder, XbeeHandlingDirector

    # CamHandlingDirectorに渡すxbeeを用意(必須ではない)
    PORT = 'COM7'  # win test
    # PORT = '/dev/ttyUSB0'
    BAUD = 9600
    ADDR_LOW = '41664D3E'
    xb_dev = XbeeSenderDevice(PORT, BAUD)
    xb_hb = XbeeHandlingBuilder(xb_dev)
    # xb_dc = XbeeHandlingDirector(xb_hb, dst_address=ADDR_LOW)
    xb_dc = XbeeHandlingDirector(xb_hb)  # アドレスを指定しない場合はcoordinatorに送信

    # cap_dev = CaptureDevice(cam_num=-1)
    cap_dev = CaptureDevice(cam_num=0)  # win test
    cap_hd = CamHandlingBuilder(cap_dev)

    # linuxのsystemd経由で動かす場合はパス指定が必須になる
    # dir_path = '/mnt/hdd/satahdd'
    # dir_path = '/home/seisan/prg/state_watcher/'

    # cap_dc = CamHandlingDirector(cap_hd,dir_path=dir_path)
    # cap_dc = CamHandlingDirector(cap_hd)
    cap_dc = CamHandlingDirector(cap_hd, xbee_dr=xb_dc)
    # cap_dc = CamHandlingDirector(cap_hd, xbee_dr=xb_dc, dir_path=dir_path)

    # with PollingServer(polling_hz=1 / 30, cap=cap_dc) as sp:
    with PollingServer(polling_hz=1 / 2, cap=cap_dc) as sp:
        # windows の場合は(Ctrl+PauseもしくはCtrl+C)で止まる
        try:
            IOLoop.instance().start()
        except KeyboardInterrupt:
            IOLoop.instance().stop()
