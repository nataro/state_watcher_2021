from logging import getLogger, StreamHandler, DEBUG

import serial
from typing import Optional, AnyStr, Final

from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice, XBee64BitAddress, XBeeException

from dev.callable_device import CallableDevice

# ライブラリ側でのロギング設定
logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)


class XbeeSenderDevice(CallableDevice):
    """
    See xbee lib manual.
    https://readthedocs.org/projects/xbplib/downloads/pdf/latest/
    https://xbplib.readthedocs.io/en/latest/index.html
    """
    ADDRESS_HIGH: Final = '0013A200'

    def __init__(self, port: str = 'COM1', baud_rate: int = 9600):
        self._port = port
        self._baud_rate = baud_rate
        # デバイスが存在しなくても'digi.xbee.devices.XBeeDevice'が生成される
        self._local_dev = XBeeDevice(self._port, self._baud_rate)

    def __call__(self, data: Optional[AnyStr], address_low: Optional[AnyStr] = None) -> Optional[bool]:
        if data is None:
            logger.debug(f'xbee send data is empty.')
            return False

        if not self.is_open():
            self.open()
        if self.is_open():
            if address_low is None:
                return self.send_data(data, '0000000000000000')  # coordinatorに送信
                # return self.send_broadcast(data)  # ←ブロードキャストは負荷が増えるから勝手に使っちゃだめ
            else:
                return self.send_remotedevice(data, address_low)
        return False

    def send_data(self, data: AnyStr, address_64bit_str: AnyStr = '0000000000000000') -> bool:
        """
        :param data:
        :param address_64bit_str:`0000000000000000`はcoordinatorのアドレス
        """
        try:
            # Instantiate a remote XBee dev object.
            remote_device = RemoteXBeeDevice(
                self._local_dev,
                XBee64BitAddress.from_hex_string(address_64bit_str)
            )
            # Send data using the remote object.
            self._local_dev.send_data(remote_device, data)
            return True
        except ValueError as e:
            logger.error(f"{self.__class__.__name__} : xbee send error. {e}")
            return False
        except XBeeException as e:
            logger.error(f"{self.__class__.__name__} : xbee send error. {e}")
            return False

    def send_broadcast(self, data: AnyStr) -> bool:
        try:
            self._local_dev.send_data_broadcast(data)
            return True
        except ValueError as e:
            logger.error(f"{self.__class__.__name__} : xbee broadcast error. {e}")
            return False
        except XBeeException as e:
            logger.error(f"{self.__class__.__name__} : xbee broadcast error. {e}")
            return False
        # except Exception as e:
        #     logger.error(f"{self.__class__.__name__} : xbee broadcast error. {e}")
        #     return False

    def send_remotedevice(self, data: AnyStr, address_low: AnyStr = '00000000') -> bool:
        """
        :param data:
        :param address_low: 下8桁のアドレスを文字列で指定
        """
        address = self.ADDRESS_HIGH + address_low
        return self.send_data(data, address)

    def open(self) -> bool:
        return self.open_local_dev()

    def close(self) -> None:
        self.close_local_dev()

    def is_open(self):
        return self._local_dev.is_open()

    def open_local_dev(self) -> bool:
        if self.is_open():
            return True
        try:
            self._local_dev.open()
            return True
        except serial.SerialException as e:
            logger.error(f"{self.__class__.__name__} : xbee open error. {e}")
            self._local_dev.close()  # openに失敗したときはとりあえずcloseしといた方がよさそう
            return False

    def close_local_dev(self) -> None:
        if self.is_open():
            self._local_dev.close()
