from logging import getLogger, StreamHandler, DEBUG

from typing import Optional

import cv2
import numpy as np

from dev.callable_device import CallableDevice

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)


class CaptureDevice(CallableDevice):

    def __init__(self, cam_num: int = 0):
        self._cam_num = cam_num
        self._video_cap = None
        self._is_open = False

    def __call__(self, **kwargs) -> Optional[np.ndarray]:
        if not self._is_open:
            self.open()
        if self._is_open:
            ret, frame = self._video_cap.read()
            if ret:
                return frame
        return None

    def open(self) -> bool:
        return self.open_cam()

    def close(self) -> None:
        return self.close_cam()

    def cam_capture(self, width: int = 1920, height: int = 1080, fps: int = 12) -> cv2.VideoCapture:
        # cap = cv2.VideoCapture(self._cam_num, cv2.CAP_DSHOW)
        cap = cv2.VideoCapture(self._cam_num)
        cap.set(cv2.CAP_PROP_FPS, fps)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        # cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('H', '2', '6', '4'))
        # cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('Y', 'U', 'Y', 'V'))
        # バッファサイズを小さくして遅延を少なくする
        cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        '''バッファサイズを小さくしても遅延が激しいようであれば、多分read()を回し続けて、
        最新の画像を取り込めるような工夫をしないと行けないと思う'''

        return cap

    def open_cam(self):
        self._video_cap = self.cam_capture()
        self._is_open = False if self._video_cap is None else self._video_cap.isOpened()
        return self._is_open

    def close_cam(self) -> None:
        if self._video_cap:
            self._video_cap.release()
            cv2.destroyAllWindows()  # Handles the releasing of the camera accordingly
            self._is_open = False
