import csv
from typing import Sequence, Union, List, Tuple, Optional

import cv2
import numpy as np


def load_parameter(file_name):
    """
    csvデータを読み込んで文字列をfloatに変換して配列に格納したものを返す
    :param file_name:
    :return:
    """
    with open(file_name) as f:
        reader = csv.reader(f)
        return [[float(s) for s in row] for row in reader]


# def transPos(trans_mat, target_pos):
def trans_pos(trans_mat: np.array, target_pos: np.array) -> np.array:
    """
    透視投影の座標変換を行う。具体的な数値の遷移は以下参照。

    trans_mat:
    [ 3.15652152e+00 -7.16209823e-01 -1.66453805e+03]
    [-3.08903449e-01  3.93942752e+00 -1.40031748e+03]
    [-4.23401823e-04  5.10348019e-04  1.00000000e+00]

    target_pos:
    [516.5 409.5]

    np.append(target_pos,1):
    [516.5 409.5   1. ]

    trans_mat@target_pos:
    [-327.48260978   53.32945487    0.99030047]

    target_pos_trans[2]:
    0.9903004725337483

    target_pos_trans/target_pos_trans[2]:
    [-330.69014795   53.85179181    1.        ]

    """
    target_pos = np.append(target_pos, 1)
    target_pos_trans = trans_mat @ target_pos
    target_pos_trans = target_pos_trans / target_pos_trans[2]
    return target_pos_trans[:2]


def camera2screen(camera_mat: np.array, world_coord: np.array) -> np.array:
    '''
    カメラ座標系の3次元座標を画像座標系の2次元座標に変換
    キャリブレーションで求めた3x3のカメラマトリックスを使用する

    camera_mat=np.asarray(
        [[1.33862088e+03, 0.00000000e+00, 9.85690260e+02],
        [0.00000000e+00, 1.39737705e+03, 4.92773839e+02],
        [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

    world_coord=np.asarray([0.1, 0.2, 0.5 ])

    image_pos = camera_mat@world_coord:
    [6.26707218e+02 5.25862329e+02 5.00000000e-01]

    image_pos/image_pos[2]:
    [1.25341444e+03 1.05172466e+03 1.00000000e+00]

    '''
    image_pos = camera_mat @ world_coord
    image_pos = image_pos / image_pos[2]
    return image_pos[:2]


def screen2camera(camera_mat: np.array, screen_coord: np.array, camera_z_code: float = 1.0) -> np.array:
    """
    画像座標系（2次元）をカメラ座標系（3次元）に変換。
    変換時にカメラ座標系のZ座標のみ指定する必要がある。
    キャリブレーションで求めた3x3のカメラマトリックスを使用する。

    camera_mat=np.asarray(
        [[1.33862088e+03, 0.00000000e+00, 9.85690260e+02],
        [0.00000000e+00, 1.39737705e+03, 4.92773839e+02],
        [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

    screen_coord=np.asarray([1.25341444e+03, 1.05172466e+03])

    screen_pos=np.append(screen_coord,1.0):
    [1.25341444e+03, 1.05172466e+03, 1.00000000e+00]

    screen_pos *= camera_z_code:(z=0.5とする)
    [6.26707218e+02, 5.25862329e+02, 5.00000000e-01]

    camera_coord=np.linalg.inv(camera_mat)@screen_pos:
    [0.1, 0.2, 0.5 ]

    """
    screen_pos = np.append(screen_coord, 1.0)
    screen_pos *= camera_z_code
    camera_coord = np.linalg.inv(camera_mat) @ screen_pos
    return camera_coord


'''マーカボード基準変換用'''


def conv_rotation_transration(r_ref, t_ref, r_targ, t_targ):
    '''
    refはreference座標、targはtarget座標の意
    回転行列は3x3次元ベクトル、並進行列は3x1もしくは1x3次元ベクトルで受けるとする
    引数の回転、並進ベクトルは、各座標をカメラ座標系に変換するときに用いるベクトルと考えることにする
    これを、target座標の原点をreference座標系に変換するときの回転、並進ベクトルを得る
    '''
    t_ref = t_ref.reshape(3, 1)
    t_targ = t_targ.reshape(3, 1)

    R = np.linalg.inv(r_ref) @ r_targ
    T = np.linalg.inv(r_ref) @ (t_targ - t_ref)

    return R, T


def rotation2euler(rvec):
    '''
    3x3回転行列を元にオイラー角を得る関数
    decomposeProjectionMatrixは射影行列を分解して、内部、外部パラメータを推定する関数だが
    3x3回転行列の最後列に[0]の列を追加した3x4の行列を射影行列に見立てて渡すことで、
    内部パラメータの影響がほぼ0のデータとして推定できるみたい？？
    これを利用して、3x3回転行列→オイラー角変換関数として利用できるのでは？
    '''
    a = np.array([[0], [0], [0]])
    proj_matrix = np.concatenate([rvec, a], 1)
    cameraMatrix, rotMatrix, transVect, rotMatrixX, rotMatrixY, rotMatrixZ, eulerAngles = cv2.decomposeProjectionMatrix(
        proj_matrix)
    return eulerAngles


def coordinate_transform(single_marker_info: tuple, org_info: tuple) -> tuple:
    """
    single_marker_info = (rvec, tvec, corner)
    org_info = (rvec_org, tvec_org)
    基準座標のtvec_oとrvec_oと、変換対象となる座標のtvec,rvec,cornerを与え、
    基準座標を原点とした結果を返す
    :param single_marker_info:
    :param org_info:
    :return: (coords, eulers)
    coords = (x, y, z)
    eulers = (rot_x, rot_y, rot_z)
    """
    rvec, tvec, corner = single_marker_info
    rvec_org, tvec_org = org_info

    rvec_s = cv2.Rodrigues(rvec)
    rvec_org_s = cv2.Rodrigues(rvec_org)
    R, T = conv_rotation_transration(rvec_org_s[0], tvec_org, rvec_s[0], tvec)
    s_s = np.array([[0], [0], [0]])  # 原点
    s_o = R @ s_s + T  # 原点の変換先

    euler = rotation2euler(R)  # 回転行列をオイラー角に変換

    coords = (s_o[0][0], s_o[1][0], s_o[2][0])
    eulers = (euler[0][0], euler[1][0], euler[2][0])

    return coords, eulers


class ArClipper(object):
    def __init__(self,
                 # coordinate=(-0.02, -0.65), width=0.45, height=0.45,
                 coordinate=(-0.02, -0.65), width=0.25, height=0.45,
                 # image_size=(224, 224),
                 image_size=224,  # specify width size
                 cam_matrix_file='./utils/mtx.csv',
                 cam_distortion_file='./utils/dist.csv',
                 # cam_matrix_file='/home/seisan/prg/state_watcher/utils/mtx.csv',
                 # cam_distortion_file='/home/seisan/prg/state_watcher/utils/dist.csv',
                 ):
        """

        :param coordinate:  検出したARマーカーを原点とし、どの座標を切り抜くか（m指定）
        :param width: 切り取る画像の幅（m指定）
        :param height: 切り取る画像の高さ（m指定）
        :param image_size: width の実サイズのみ指定するようにする
        :param cam_matrix_file:
        :param cam_distortion_file:
        """
        z = 0
        x, y = coordinate
        w, h = width, height
        # 切り取る範囲の座標情報（m単位）
        self._clipping_area = [
            [[x], [y], [z]],
            [[x + w], [y], [z]],
            [[x], [y + h], [z]],
            [[x + w], [y + h], [z]]
        ]

        # 切り取る画像のサイズ（画素）
        # self._clipped_width, self._clipped_height = image_size
        self._clipped_width = image_size
        self._clipped_height = int(self._clipped_width * h / w)

        # 検出するマーカーボード（基準座標とする予定）の設定
        self._markers_x = 6  # マーカーボードのx方向に配置したマーカー数
        self._markers_y = 1  # マーカーボードのy方向に配置するマーカー数
        self._marker_length = 0.03  # マーカーのサイズ 画素数ではない。メータ指定
        self._marker_separation = 0.005  # マーカー間の間隔
        self._first_marker_id = 100  # 検出するマーカーボードの一番最初のマーカのidを指定

        # arucoライブラリ
        self._aruco = cv2.aruco
        # 使用するARマーカーのdictionary
        self._dictionary = self._aruco.getPredefinedDictionary(self._aruco.DICT_4X4_250)
        # 検出するマーカーボード
        self._grid_board = self._aruco.GridBoard_create(
            self._markers_x,
            self._markers_y,
            self._marker_length,
            self._marker_separation,
            self._dictionary,
            self._first_marker_id
        )
        # aruco.detectMarkers()実行時に渡すパラメータ（必須ではない？？）
        self._parameters = self._aruco.DetectorParameters_create()
        # CORNER_REFINE_NONE, no refinement.
        # CORNER_REFINE_SUBPIX, do subpixel refinement.
        # CORNER_REFINE_CONTOUR use contour-Points
        self._parameters.cornerRefinementMethod = self._aruco.CORNER_REFINE_CONTOUR

        # カメラパラメータの読み込み
        self._camera_matrix = np.asarray(load_parameter(cam_matrix_file))
        self._camera_dist = np.asarray(load_parameter(cam_distortion_file))

        # aruco.detectMarkersで検出したcornersとidを格納する
        self._corners = []
        self._ids = []
        # 検出したマーカーボードに関する情報を格納する
        self._retval = 0
        self._rvec_org = None
        self._tvec_org = None

    def update_detect_markers(self, gray: np.ndarray):
        """
        ・マーカを検出し、そのrotation vectorとtranslation vectorをメンバ変数に保持する
            -> self._corners, self._ids
        ・上記検出したマーカから、基準となるマーカボードを検出し、そのrotation vectorとtranslation vectorをメンバ変数に保持する
            -> self._retval, self._rvec_org, self._tvec_org
        :param gray: グレースケール画像
        """
        # とりあえずマーカー類を全部個別に検出
        # print(rejected_ImgPoints) # 正しく検出できなかったマーカーの情報？デバッグ目的
        corners, ids, rejected_ImgPoints = self._aruco.detectMarkers(
            gray,
            self._dictionary,
            parameters=self._parameters
        )
        self._corners = corners
        self._ids = ids

        # 検出した全マーカーを対象として、特定のーカーボードの位置推定
        # opencv4以降引数が増えている
        # https://docs.opencv.org/4.1.1/d9/d6a/group__aruco.html#ga366993d29fdddd995fba8c2e6ca811ea
        self._retval, self._rvec_org, self._tvec_org = self._aruco.estimatePoseBoard(
            self._corners,
            self._ids,
            self._grid_board,
            self._camera_matrix,
            self._camera_dist,
            None,
            None
        )

    def get_estimate_board(self):
        return self._retval, self._rvec_org, self._tvec_org

    # def detect_single_markers(self, gray: np.ndarray, ) -> List:
    def detect_single_markers(self) -> List:
        """

        工具管理のためにシングルマーカ処理を追加途中（20210813）

        aruco.estimatePoseSingleMarkers()では、マーカーの中心をマーカー座標系の原点として結果を出力していると思われる

        Parameters
        ----------
        gray

        Returns
        -------

        """

        detect_markers = []

        # if len(corners) <= 0:
        if not self._corners:  # 検出されなかったときは空のリストが返る
            return detect_markers

        # マーカーボードのidは除外しておく
        board_ids = range(
            self._first_marker_id,
            self._first_marker_id + (self._markers_x * self._markers_y)
        )
        single_corners = [c for i, c in zip(self._ids, self._corners) if i not in board_ids]
        single_ids = np.array([i for i in self._ids if i not in board_ids])

        if not single_corners:  # 検出されなかったときは空のリストが返る
            return detect_markers

        for corner, id in zip(single_corners, single_ids):
            rvec, tvec, _ = self._aruco.estimatePoseSingleMarkers(
                corner,
                self._marker_length,
                self._camera_matrix,
                self._camera_dist,
                None, None
            )

            # 不要なaxisを除去
            tvec = np.squeeze(tvec)
            rvec = np.squeeze(rvec)
            corner = np.squeeze(corner)

            detect_markers.append((corner, id, tvec, rvec))

        return detect_markers

    @staticmethod
    def coordinate_transform(single_marker_info: tuple, org_info: tuple) -> tuple:
        return coordinate_transform(single_marker_info, org_info)

    def roi_coordinates(self, rvec_org, tvec_org):
        """
        clippingする範囲の画像座標（ROI）を取得する
        :param rvec_org: rotation vector
        :param tvec_org: translation vector
        :return: Region of Interest 切り取り範囲（4端点：画像座標系）
        """
        rvec_o = cv2.Rodrigues(rvec_org)
        rvec_o = rvec_o[0]

        # 切り取り範囲(マーカー座標系)をカメラ座標系の座標に変換
        cut_area_cam = [rvec_o @ np.array(m) + tvec_org for m in self._clipping_area]

        # カメラ座標を画像座標に変換
        roi = [camera2screen(self._camera_matrix, c) for c in cut_area_cam]

        return roi

    def image_clipping(self, img: np.ndarray, roi: Sequence[Union[int, float]]) -> np.ndarray:
        """
        :param img:
        :param roi: Region of Interest 切り取り範囲（4端点：画像座標系）
        :return: roiを切り取って、指定したサイズの矩形に投影した画像
        """
        # 画像を切り取る
        roi_coordinates = np.float32(roi)
        true_coordinates = np.float32([
            [0., 0.],
            [self._clipped_width, 0.],
            [0., self._clipped_height],
            [self._clipped_width, self._clipped_height]
        ])
        trans_mat = cv2.getPerspectiveTransform(roi_coordinates, true_coordinates)
        img_trans = cv2.warpPerspective(img, trans_mat, (self._clipped_width, self._clipped_height))
        return img_trans

    def draw_clipping_frame(self, img: np.ndarray, roi: Sequence[Union[int, float]], rvec_org, tvec_org):
        """
        画像に原点軸やクリッピングする枠など描画する
        :param img: 描画対象となるopencvでキャプチャした画像
        :param roi: Region of Interest 切り取り範囲（4端点：画像座標系）
        :param rvec_org:
        :param tvec_org:
        :return:
        """
        # 検出したマーカーボードの原点座標を描画する
        axis_length = 0.1
        self._aruco.drawAxis(img, self._camera_matrix, self._camera_dist,
                             rvec_org, tvec_org, axis_length)

        # convexHullで輪郭の座標を順番通りに取得して、元画像に枠を描画
        roi_int = np.array(roi, dtype=np.int32)
        hull = cv2.convexHull(roi_int, returnPoints=True)
        cv2.drawContours(img, [hull], -1, (0, 255, 255), 2)  # 「-1」は画像中の全輪郭を描画する

    def get_origin_clipped_image(self, img: Optional[np.ndarray]) -> Optional[np.ndarray]:
        """
        事前にself.update_detect_markers()を実行し、検出したマーカーやマーカーボードの情報をメンバ変数に保持させておくこと。
        検出したシングルマーカの座標を、マーカーボード基準の座標に変換し、その座標、姿勢、id情報を返す。
        マーカボードを基準として指定した範囲での切り取りを行う
        :param img: 入力画像
        :return: clipped_image: 切取り画像
        """
        if self._retval != 0:  #
            # clippingする範囲の画像座標（ROI）を取得する
            roi = self.roi_coordinates(self._rvec_org, self._tvec_org)

            # 画像を切り取る
            clipped_img = self.image_clipping(img, roi)

            return clipped_img

        return None

    def get_origin_single_markers(self) -> List[Tuple]:
        """
        事前にself.update_detect_markers()を実行し、検出したマーカーやマーカーボードの情報をメンバ変数に保持させておくこと。
        検出したシングルマーカの座標を、マーカーボード基準の座標に変換し、その座標、姿勢、id情報を返す。

        :return:
            detect_markers = [
                (coord, euler, id),
                ...,
                (coord, euler, id),
            ]
        """
        detect_markers = []

        if self._retval == 0: return detect_markers

        # シングルマーカーのみ
        single_markers = self.detect_single_markers()

        if not single_markers: return detect_markers

        org_info = self._rvec_org, self._tvec_org
        for corner, id, tvec, rvec in single_markers:
            single_marker_info = rvec, tvec, corner
            coord, euler = coordinate_transform(single_marker_info, org_info)
            detect_markers.append((coord, euler, id[0]))

        return detect_markers


def main():
    # 検出したマーカーボード基準で画像の切り取りを行うクラスのインスタンスを生成
    clipper = ArClipper()

    size = (1920, 1080)
    # size = (1280, 720)

    cap = cv2.VideoCapture(0)
    # cap = cv2.VideoCapture(0 + cv2.CAP_DSHOW)  # Windows:こうしないとエラーになる場合がある（特に新型のカメラ等）
    # cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # Windows:こうしないとエラーになる場合がある（特に新型のカメラ等）

    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, size[1])
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, size[0])
    print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    print(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    if cap.isOpened() is False:
        raise ("IO Error")

    while True:
        ret, img = cap.read()
        # if ret == False:
        if ret is False:
            continue

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # マーカを検出する（検出したマーカーの情報はself._ar_clipperに保持される）
        clipper.update_detect_markers(gray)

        # マーカーボードを検出し、そのrotation vectorとtranslation vectorを返す
        retval, rvec_org, tvec_org = clipper.get_estimate_board()

        if retval != 0:  #

            ''' 画像の切り取りに関する処理 '''
            # clippingする範囲の画像座標（ROI）を取得する
            roi = clipper.roi_coordinates(rvec_org, tvec_org)
            # print(rvec_org)

            # 画像を切り取る
            cliiped_img = clipper.image_clipping(img, roi)

            # cliiped_imgに対する何等かの処理をここで行う

            # 画像に原点軸やクリッピングする枠など描画する
            clipper.draw_clipping_frame(img, roi, rvec_org, tvec_org)

            '''シングルマーカーに関する処理'''
            thresh = (0.4, 999, 999)
            # シングルマーカーのみ
            single_markers = clipper.detect_single_markers()
            if single_markers:

                org_info = rvec_org, tvec_org
                corners = []
                ids = []
                for corner, id, tvec, rvec in single_markers:
                    single_marker_info = rvec, tvec, corner
                    coord, euler = clipper.coordinate_transform(single_marker_info, org_info)
                    if not (coord > thresh):  # 閾値より小さい座標が含まれる場合
                        # print(coord)
                        corners.append(corner)
                        ids.append(id[0])

                # drawDetectMarkes用にデータを成形
                corners = [np.array([c]) for c in corners]
                ids = np.array([[id] for id in ids])

                # if corners and ids:
                if len(corners) == len(ids):
                    # シングルマーカーのIDだけ描画しておく
                    cv2.aruco.drawDetectedMarkers(img, corners, ids, (0, 255, 0))

        cv2.imshow("Image", img)

        # Escキーで終了
        key = cv2.waitKey(50)
        if key == 27:  # ESC
            break

    print("finish")
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
