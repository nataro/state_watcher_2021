import time
from pathlib import Path
from typing import Sequence

import cv2
import numpy as np
import torch
import torch.nn as nn
from torchvision import models
from torchvision import transforms

"""
toch用
"""

class ClassiferMobilenet(object):
    def __init__(self,
                 path: str,
                 file_name: str,
                 size: int,
                 classes: Sequence[str],
                 mean: Sequence[int],
                 std: Sequence[int]
                 ):
        """
        :param path:
        :param file_name:
        :param size:
        :param classes:
        """
        self.__transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean, std)
        ])
        self.__path = Path(path)
        self.__file_name = file_name  # 読み込むファイル名「*.pkl」
        self.__size = size  # 入力画像 size x size
        self.__classes = classes

        # 事前学習していないモデルで良いと思う（後で重みをloadするので）
        self.__net = models.mobilenet_v2(pretrained=False)
        # mobalenet の最終出力総は net.classifier[1]でin_features=1280
        # なのでそのように修正する
        self.__net.classifier[1] = nn.Linear(in_features=1280, out_features=len(self.__classes))

        # weight の読み込み
        self.__net.load_state_dict(torch.load(self.__path / self.__file_name))
        # 評価モードにしておく
        self.__net.eval()

    def __call__(self, img: np.ndarray, *args, **kwargs):
        """
        :param img:  opencvで読み込んだ画像データを想定
        :param args:
        :param kwargs:
        :return:
        """

        # start = time.time()

        pred_class = self.predict(img)

        # process_time = time.time() - start
        # print(process_time)
        print(pred_class)

        return pred_class

    def predict(self, img: np.ndarray):
        """
        :param img: opencvで読み込んだ画像データを想定
        :return: 各クラスの確率が格納された配列
        """
        # BGR->RGBにする
        img = img[:, :, ::-1].copy()
        # print(img.shape)

        # transformで前処理しテンソルに変換
        img_t = self.__transform(img)
        # print(img_t.shape)

        # バッチ処理の次元を追加
        img_t = img_t.unsqueeze_(0)
        # print(img_t.shape)

        start = time.time()
        pred_out_t = self.__net(img_t)
        process_time = time.time() - start
        print(process_time)

        pred_out = pred_out_t.detach().numpy()
        print(pred_out)

        pred_id = np.argmax(pred_out)

        return self.__classes[pred_id]


def main():
    path = 'ml'
    model = 'trained_model_torch_mobilenet.pth'
    size = 128
    classes = ['00_nowork', '01_newwork', '02_processing', '03_handling']
    mean = (0.485, 0.456, 0.406)
    std = (0.229, 0.224, 0.225)

    net = ClassiferMobilenet(
        path=path,
        file_name=model,
        size=size,
        classes=classes,
        mean=mean,
        std=std
    )

    # opencvで画像を読み込む
    test_img = 'ml/tes_img/processing_00.jpg'
    img = cv2.imread(test_img)
    img = cv2.resize(img, (size, size))

    # モデルでクラスを予測
    pred_class = net(img)


if __name__ == "__main__":
    main()
